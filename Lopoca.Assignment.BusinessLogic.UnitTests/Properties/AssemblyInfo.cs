using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Lopoca.Assignment.BusinessLogic.UnitTests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("rg-adguard")]
[assembly: AssemblyProduct("Lopoca.Assignment.BusinessLogic.UnitTests")]
[assembly: AssemblyCopyright("Copyright © rg-adguard 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("adf48ef8-6cd5-43e1-bf60-56ab25b3e311")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
