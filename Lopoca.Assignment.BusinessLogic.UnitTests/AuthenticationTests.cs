﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lopoca.Assignment.Data.Contracts;
using Moq;
using AutoMapper;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;

namespace Lopoca.Assignment.BusinessLogic.UnitTests
{
    [TestClass]
    public class AuthenticationTests
    {
        private Mapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.Contracts.Models.Session, BusinessLogic.Contracts.Models.Session>();
                cfg.CreateMap<BusinessLogic.Contracts.Models.Session, Data.Contracts.Models.Session>();
                cfg.CreateMap<Data.Contracts.Models.User, BusinessLogic.Contracts.Models.User>();
                cfg.CreateMap<BusinessLogic.Contracts.Models.User, Data.Contracts.Models.User>();
                cfg.CreateMap<Data.Contracts.Models.FileData, BusinessLogic.Contracts.Models.File>();
                cfg.CreateMap<BusinessLogic.Contracts.Models.File, Data.Contracts.Models.FileData > ();
            });

            return new Mapper(config);
        }

        private string HashString(string s)
        {
            var encrypt = SHA256.Create();
            return Convert.ToBase64String(encrypt.ComputeHash(UTF8Encoding.UTF8.GetBytes(s)));
        }
        [TestMethod]
        public async Task CheckToken_Valid_sessionId_Test()
        {
            Moq.Mock<ISessionRepository> sessionRepo = new Moq.Mock<ISessionRepository>();
            Moq.Mock<IUserRepository> userRepo = new Moq.Mock<IUserRepository>();
            var session = Guid.NewGuid();
            var userId = Guid.NewGuid();

            sessionRepo.Setup(x => x.Get(session)).ReturnsAsync(
            new Data.Contracts.Models.Session
            {
                Id = session,
                Expiry = DateTime.UtcNow.AddMinutes(20),
                UserId = userId
            }
            );

            userRepo.Setup(x => x.Get(userId)).ReturnsAsync(new Data.Contracts.Models.User()
            {
                Id = userId,
                Login = "test",
                Name = "test",
                Password = "",
                PasswordSalt = "",
                IsDeleted = false,
                Surname = "test"
            });


            var service = new AuthService(userRepo.Object, sessionRepo.Object, GetMapper(), new SHAProvider());
            var result = await service.CheckToken(session);

            Assert.IsNotNull(result);
            Assert.AreEqual(result.User.Id, userId);
            Assert.AreEqual(result.Id, session);

        }


        [TestMethod]
        public async Task CheckToken_Invalid_sessionId_Test()
        {
            Moq.Mock<ISessionRepository> sessionRepo = new Moq.Mock<ISessionRepository>();
            Moq.Mock<IUserRepository> userRepo = new Moq.Mock<IUserRepository>();
            var session = Guid.NewGuid();
            var userId = Guid.NewGuid();

            sessionRepo.Setup(x => x.Get(session)).ReturnsAsync(
            new Data.Contracts.Models.Session
            {
                Id = session,
                Expiry = DateTime.UtcNow.AddMinutes(20),
                UserId = userId
            }
            );

            userRepo.Setup(x => x.Get(userId)).ReturnsAsync(new Data.Contracts.Models.User()
            {
                Id = userId,
                Login = "test",
                Name = "test",
                Password = "",
                PasswordSalt = "",
                IsDeleted = false,
                Surname = "test"
            });


            var service = new AuthService(userRepo.Object, sessionRepo.Object, GetMapper(), new SHAProvider());
            var result = await service.CheckToken(Guid.NewGuid());

            Assert.IsNull(result);

        }

        [TestMethod]
        public async Task ExpireSession_Test()
        {
            Moq.Mock<ISessionRepository> sessionRepo = new Moq.Mock<ISessionRepository>();
            Moq.Mock<IUserRepository> userRepo = new Moq.Mock<IUserRepository>();
            var session = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var sessionObj = new BusinessLogic.Contracts.Models.Session
            {
                Id = session,
                Expiry = DateTime.UtcNow.AddMinutes(20)
            };

            sessionRepo.Setup(x => x.Update(It.IsAny<Data.Contracts.Models.Session>())).Callback<Data.Contracts.Models.Session>((s) =>
              {
                  Assert.IsTrue(s.Expiry < DateTime.UtcNow);
              }).Returns(Task.CompletedTask);

            var service = new AuthService(userRepo.Object, sessionRepo.Object, GetMapper(), new SHAProvider());
            var result = await service.ExpireSession(sessionObj);

            Assert.IsTrue(result.Expiry < DateTime.UtcNow);

        }

        [TestMethod]
        public async Task ExtendSession_Test()
        {
            Moq.Mock<ISessionRepository> sessionRepo = new Moq.Mock<ISessionRepository>();
            Moq.Mock<IUserRepository> userRepo = new Moq.Mock<IUserRepository>();
            var session = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var sessionObj = new BusinessLogic.Contracts.Models.Session
            {
                Id = session,
                Expiry = DateTime.UtcNow
            };

            sessionRepo.Setup(x => x.Update(It.IsAny<Data.Contracts.Models.Session>())).Callback<Data.Contracts.Models.Session>((s) =>
            {
                Assert.IsTrue(s.Expiry > DateTime.UtcNow);
            }).Returns(Task.CompletedTask);

            var service = new AuthService(userRepo.Object, sessionRepo.Object, GetMapper(), new SHAProvider());
            await service.ExtendSession(sessionObj.Id,60);

        }


        [TestMethod]
        public async Task Login_valid_credentials_Test()
        {
            Moq.Mock<ISessionRepository> sessionRepo = new Moq.Mock<ISessionRepository>();
            Moq.Mock<IUserRepository> userRepo = new Moq.Mock<IUserRepository>();
            var session = Guid.NewGuid();
            var userId = Guid.NewGuid();
            
            var username = "test";
            var password = "test";
            var passwordSalt = HashString("rand0mSalt");

            userRepo.Setup(x => x.Get(username)).ReturnsAsync(new Data.Contracts.Models.User
            {
                Id = Guid.NewGuid(),
                Login = username,
                PasswordSalt = passwordSalt,
                Password = HashString(password+passwordSalt)
            });
            

            var service = new AuthService(userRepo.Object, sessionRepo.Object, GetMapper(), new SHAProvider());
            var result = await service.Login(username, password);

            Assert.IsNotNull(result);

        }



        [TestMethod]
        public async Task Login_invalid_credentials_Test()
        {
            Moq.Mock<ISessionRepository> sessionRepo = new Moq.Mock<ISessionRepository>();
            Moq.Mock<IUserRepository> userRepo = new Moq.Mock<IUserRepository>();
            var session = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var username = "test";
            var password = "test";
            var passwordSalt = HashString("rand0mSalt");

            userRepo.Setup(x => x.Get(username)).ReturnsAsync(new Data.Contracts.Models.User
            {
                Id = Guid.NewGuid(),
                Login = username,
                PasswordSalt = passwordSalt,
                Password = HashString(password + passwordSalt)
            });


            var service = new AuthService(userRepo.Object, sessionRepo.Object, GetMapper(), new SHAProvider());
            var result = await service.Login(username, "wrongpass");

            Assert.IsNull(result);

        }
    }
}
