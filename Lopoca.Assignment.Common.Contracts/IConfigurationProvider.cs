﻿using System;

namespace Lopoca.Assignment.Common
{
    public interface IConfigurationProvider
    {
        string GetSetting(string key);
        string GetConnectionString(string key);
    }
}
