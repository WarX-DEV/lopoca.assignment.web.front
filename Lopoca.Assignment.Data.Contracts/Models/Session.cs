﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lopoca.Assignment.Data.Contracts.Models
{
    public class Session
    {
        public Guid Id { get; set; }
        public DateTime Expiry { get; set; }
        public Guid UserId { get; set; }
    }
}
