﻿using System;

namespace Lopoca.Assignment.Data.Contracts.Models
{
    public class FileData
    {
        public Guid Id { get; set; } 
        public string GivenFilename { get; set; }
        public string Extension { get; set; }
        public Guid UserId { get; set; }
        public byte[] Content { get; set; }
    }
}