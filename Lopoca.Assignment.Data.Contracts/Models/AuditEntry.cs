﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lopoca.Assignment.Data.Contracts.Models
{
    public class AuditEntry
    {
        public Guid Id { get; set; } = new Guid();
        public DateTime Timestamp { get; set; } = DateTime.UtcNow;
        public string Message { get; set; }
        public Guid AffectedObject { get; set; }
    }
}
