﻿using System;

namespace Lopoca.Assignment.Data.Contracts.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public bool IsDeleted { get; set; }
    }
}