﻿using Lopoca.Assignment.Data.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lopoca.Assignment.Data.Contracts
{
    public interface IFileRepository
    {
        Task<FileData> Insert(FileData session);
        Task<FileData> Update(FileData session);
        Task Delete(Guid id);
        Task<FileData> Get(Guid id);
        Task<byte[]> GetContent(Guid id);
        Task<IEnumerable<FileData>> GetByUser(Guid userId);
    }
}
