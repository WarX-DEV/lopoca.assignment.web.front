﻿using Lopoca.Assignment.Data.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lopoca.Assignment.Data.Contracts
{
    public interface IUserRepository
    {
        Task Insert(User user);
        Task Update(User session);
        Task Delete(Guid id);
        Task<User> Get(Guid id);
        Task<User> Get(string login);
    }
}
