﻿using Lopoca.Assignment.Data.Contracts.Models;
using System;
using System.Threading.Tasks;

namespace Lopoca.Assignment.Data.Contracts
{
    public interface ISessionRepository
    {
        Task Insert(Session session);
        Task Update(Session session);
        Task Delete(Guid id);
        Task<Session> Get(Guid id);
    }
}
