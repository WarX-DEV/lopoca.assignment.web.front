﻿using Lopoca.Assignment.Data.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lopoca.Assignment.Data.Contracts
{
    public interface IAuditRepository
    {
        Task Log(AuditEntry auditEntry);
    }
}
