﻿using Lopoca.Assignment.BusinessLogic.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using Lopoca.Assignment.BusinessLogic.Contracts.Models;
using System.Threading.Tasks;
using Lopoca.Assignment.Data.Contracts;
using AutoMapper;
using System.Linq;
using Lopoca.Assignment.Data.Contracts.Models;

namespace Lopoca.Assignment.BusinessLogic
{
    public class CsvFileService : IFileService
    {
        private readonly IFileRepository _fileRepository;

        public IMapper _mapper { get; }

        public CsvFileService(IFileRepository fileRepository,IMapper mapper)
        {
            _fileRepository = fileRepository;
            _mapper = mapper;
        }
        public async Task<File> GetFile(Guid id)
        {
            return _mapper.Map<File>(await _fileRepository.Get(id));
        }

        public async Task<byte[]> GetFileContent(Guid id)
        {
            return await _fileRepository.GetContent(id);
        }

        public async Task<IEnumerable<File>> GetFilesForUser(Guid id)
        {
            var files = _mapper.Map<IEnumerable<File>>(await _fileRepository.GetByUser(id));
            return files;
        }

        public async Task<File> SaveFile(File file)
        {
            if (file.Extension != "csv")
                throw new ArgumentException("File is not a csv");

            if(file.Id== Guid.Empty)
            {
                return _mapper.Map<File>(await _fileRepository.Insert(_mapper.Map<FileData>(file)));
            }
            else
            {
                return _mapper.Map<File>(await _fileRepository.Update(_mapper.Map<FileData>(file)));
            }
        }
    }
}
