﻿using Lopoca.Assignment.BusinessLogic.Contracts;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Lopoca.Assignment.BusinessLogic
{
    public class SHAProvider : IEncryptionProvider
    {
        private readonly SHA256 _encryptMethod;

        public SHAProvider()
        {
            _encryptMethod = SHA256.Create();
        }
        public string Encrypt(string encrypt)
        {
            return Convert.ToBase64String(_encryptMethod.ComputeHash(UTF8Encoding.UTF8.GetBytes(encrypt)));
        }
    }
}
