﻿using Lopoca.Assignment.BusinessLogic.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using Lopoca.Assignment.BusinessLogic.Contracts.Models;
using System.Threading.Tasks;
using Lopoca.Assignment.Data.Contracts;
using AutoMapper;
using System.Security.Cryptography;

namespace Lopoca.Assignment.BusinessLogic
{
    public class AuthService : IAuthService
    {
        private IUserRepository _userRepository;
        private readonly ISessionRepository _sessionRepository;
        private readonly IMapper _mapper;
        private readonly IEncryptionProvider _encryptionProvider;

        public AuthService(IUserRepository userRepository, ISessionRepository sessionRepository, IMapper mapper,IEncryptionProvider encryptionProvider)
        {
            _userRepository = userRepository;
            _sessionRepository = sessionRepository;
            _mapper = mapper;
            _encryptionProvider = encryptionProvider;
        }
        public async Task<Session> CheckToken(Guid id)
        {
            var session = await _sessionRepository.Get(id);
            if (session != null && session.Expiry>DateTime.UtcNow)
            {
                var retSession = _mapper.Map<Session>(session);
                retSession.User = _mapper.Map<User>(await _userRepository.Get(session.UserId));
                return retSession;
            }
            return null;
        }

        public async Task<Session> ExpireSession(Session session)
        {
            session.Expiry = DateTime.UtcNow.AddDays(-1);
            await _sessionRepository.Update(_mapper.Map<Data.Contracts.Models.Session>(session));
            return session;
        }

        public async Task ExtendSession(Guid session, int minutes)
        {
            await _sessionRepository.Update(new Data.Contracts.Models.Session
            {
                Id = session,
                Expiry = DateTime.UtcNow.AddMinutes(minutes)
            });
        }

        public async Task<Session> Login(string username, string password)
        {

            var user = await _userRepository.Get(username);
            if(user != null)
            {
                //user exists
                var encrpyt = SHA256.Create();
                var encPassword = password + user.PasswordSalt;
                var hashedPass = _encryptionProvider.Encrypt(encPassword);

                if(hashedPass == user.Password)
                {
                    var session = new Data.Contracts.Models.Session() { UserId = user.Id, Expiry = DateTime.UtcNow.AddMinutes(15), Id = Guid.NewGuid() };
                    await _sessionRepository.Insert(session);
                    var retSession = _mapper.Map<Session>(session);
                    retSession.User = _mapper.Map<User>(user);
                    return retSession;
                }
            }
            return null;
        }
    }
}
