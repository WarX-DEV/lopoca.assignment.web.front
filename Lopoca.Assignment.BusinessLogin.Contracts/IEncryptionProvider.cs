﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lopoca.Assignment.BusinessLogic.Contracts
{
    public interface IEncryptionProvider
    {
        string Encrypt(string encrypt);
    }
}
