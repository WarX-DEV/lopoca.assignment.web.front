﻿using Lopoca.Assignment.BusinessLogic.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lopoca.Assignment.BusinessLogic.Contracts
{
    public interface IFileService
    {
        Task<IEnumerable<File>> GetFilesForUser(Guid id);
        Task<File> SaveFile(File file);
        Task<File> GetFile(Guid id);
        Task<byte[]> GetFileContent(Guid id);
    }
}
