﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lopoca.Assignment.BusinessLogic.Contracts.Models
{
    public class Session
    {
        public Guid Id { get; set; }
        public DateTime Expiry { get; set; }
        public User User { get; set; }
    }
}
