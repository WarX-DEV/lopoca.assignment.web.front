﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lopoca.Assignment.BusinessLogic.Contracts.Models
{
    public class File
    {
        public Guid Id { get; set; }
        public string GivenFilename { get; set; }
        public string Extension { get; set; }
        public byte[] Content { get; set; }
        public Guid UserId { get; set; }
    }
}
