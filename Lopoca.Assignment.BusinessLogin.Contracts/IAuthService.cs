﻿using Lopoca.Assignment.BusinessLogic.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lopoca.Assignment.BusinessLogic.Contracts
{
    public interface IAuthService
    {
        Task<Session> Login(string username, string surname);
        Task<Session> CheckToken(Guid id);
        Task ExtendSession(Guid session, int minutes);
        Task<Session> ExpireSession(Session session);
    }
}
