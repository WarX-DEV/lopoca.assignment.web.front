﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lopoca.Assignment.Web.Front.Models
{
    public class FileStatusViewModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public Guid FileId { get; set; }
    }
}