﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lopoca.Assignment.Web.Front.Models
{
    public class FileUploadViewModel
    {
        public Guid Id { get; set; }
        public string GivenFilename { get; set; }
        public IFormFile UploadedFile { get; set; }
    }
}
