﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Lopoca.Assignment.Web.Middleware;
using Lopoca.Assignment.Data.Contracts;
using Lopoca.Assignment.Data.EF;
using Lopoca.Assignment.BusinessLogic.Contracts;
using Lopoca.Assignment.BusinessLogic;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Lopoca.Assignment.Web.Services;
using Microsoft.AspNetCore.Http;
using Lopoca.Assignment.Web.Front.AppData;
using Lopoca.Assignment.Data.EF.Models;
using Lopoca.Assignment.BusinessLogic.Contracts.Models;
using Lopoca.Assignment.Web.Front.Models;
using Lopoca.Assignment.Data.Contracts.Models;

namespace Lopoca.Assignment.Web.Front
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IContainer ApplicationContainer { get; private set; }

        public IConfiguration Configuration { get; }        

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.Contracts.Models.Session, BusinessLogic.Contracts.Models.Session>();
                cfg.CreateMap<BusinessLogic.Contracts.Models.Session, Data.Contracts.Models.Session>();
                cfg.CreateMap<Data.Contracts.Models.User, BusinessLogic.Contracts.Models.User>();
                cfg.CreateMap<BusinessLogic.Contracts.Models.User, Data.Contracts.Models.User>();
                cfg.CreateMap<Data.Contracts.Models.FileData, BusinessLogic.Contracts.Models.File>();
                cfg.CreateMap<BusinessLogic.Contracts.Models.File, Data.Contracts.Models.FileData>();
                cfg.CreateMap<Data.Contracts.Models.Session, SessionDTO>();
                cfg.CreateMap<SessionDTO, Data.Contracts.Models.Session>();
                cfg.CreateMap<Data.Contracts.Models.User, UserDTO>();
                cfg.CreateMap<UserDTO, Data.Contracts.Models.User>();
                cfg.CreateMap<Data.Contracts.Models.FileData, FileDTO>();
                cfg.CreateMap<FileDTO, Data.Contracts.Models.FileData>();
                cfg.CreateMap<File, FileUploadViewModel>();
                cfg.CreateMap<FileUploadViewModel, File>();
                cfg.CreateMap<AuditDTO, AuditEntry>();
                cfg.CreateMap<AuditEntry, AuditDTO>();
            }); ;

            // Add services to the collection.
            services.AddSingleton(config.CreateMapper());            
            services.AddMvc();
            

            // Create the container builder.
            var builder = new ContainerBuilder();
            
            builder.Populate(services);
            builder.RegisterType<UserRepository>().As<IUserRepository>().SingleInstance();
            builder.RegisterType<FileRepository>().As<IFileRepository>().SingleInstance();
            builder.RegisterType<SessionRepository>().As<ISessionRepository>().SingleInstance();
            builder.RegisterType<AuthService>().As<IAuthService>().SingleInstance();
            builder.RegisterType<CsvFileService>().As<IFileService>().SingleInstance();
            builder.RegisterType<SessionManager>().As<ISessionManager>().SingleInstance();
            builder.RegisterType<SHAProvider>().As<IEncryptionProvider>().SingleInstance();
            builder.RegisterType<AuditRepository>().As<IAuditRepository>().SingleInstance();
            builder.Register(x => Configuration);
            builder.RegisterType<DbContextOptions<AssignmentContext>>().As<DbContextOptions<AssignmentContext>>().SingleInstance();
            builder.RegisterType<ConfigurationProvider>().As<Common.IConfigurationProvider>();
            builder.RegisterType<AssignmentContext>().AsSelf();
            builder.RegisterType<DataInitialiser>().AsSelf();

            this.ApplicationContainer = builder.Build();

            this.ApplicationContainer.Resolve<AssignmentContext>().Database.EnsureCreated();
            var init = this.ApplicationContainer.Resolve<DataInitialiser>();
            init.Init();

            // Create the IServiceProvider based on the container.
            return new AutofacServiceProvider(this.ApplicationContainer);
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            
            app.UseStaticFiles();
            app.UseStatusCodePagesWithReExecute("/Error/Status/{0}");
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "error",
                    template: "{controller=Error}/{action=StatusCode}/{code}");
            });
        }
    }
}
