﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Lopoca.Assignment.Web.Front.Models;
using Microsoft.AspNetCore.Authentication;
using Lopoca.Assignment.Web.Services;
using Lopoca.Assignment.BusinessLogic.Contracts;
using Lopoca.Assignment.Web.Piplines;
using Lopoca.Assignment.BusinessLogic.Contracts.Models;

namespace Lopoca.Assignment.Web.Front.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAuthService _authenticationService;
        private readonly ISessionManager _sessionManager;

        public AccountController(IAuthService authenticationService, ISessionManager sessionManager)
        {
            _authenticationService = authenticationService;
            _sessionManager = sessionManager;
        }
        public async Task<IActionResult> Login(LoginViewModel login)
        {
            var session = await _authenticationService.Login(login.Username, login.Password);
            if (session != null)
            {
                await _sessionManager.CreateSession(session);
                return Redirect("/File/List");
            }
            return Redirect("/");
        }

        [MiddlewareFilter(typeof(AuthenticatedPipline))]
        public async Task<IActionResult> Logout()
        {
            var session = this.HttpContext.Items["Session"] as Session;
            var sessionExpire = _authenticationService.ExpireSession(session);
            return Redirect("/");
        }
    }
}