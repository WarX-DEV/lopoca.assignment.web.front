﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Lopoca.Assignment.Web.Piplines;
using Lopoca.Assignment.BusinessLogic.Contracts;
using Lopoca.Assignment.BusinessLogic.Contracts.Models;
using Lopoca.Assignment.Web.Front.Models;
using System.IO;
using AutoMapper;

namespace Lopoca.Assignment.Web.Front.Controllers
{
    [MiddlewareFilter(typeof(AuthenticatedPipline))]
    public class FileController : Controller
    {
        private readonly IFileService _fileService;
        private readonly IMapper _mapper;

        public FileController(IFileService fileService,IMapper mapper)
        {
            _fileService = fileService;
            _mapper = mapper;
        }
        
        public async Task<IActionResult> List()
        {
            var user = this.HttpContext.Items["User"] as User;
            var dbfiles = await _fileService.GetFilesForUser(user.Id);
            var files = _mapper.Map<IEnumerable<FileUploadViewModel>>(dbfiles);
            return View(files);
        }

        public IActionResult Upload(Guid id)
        {
            return View(new FileUploadViewModel { Id = id });
        }

        public async Task<IActionResult> Details(Guid id)
        {
            return View(_mapper.Map<FileUploadViewModel>(await _fileService.GetFile(id)));
        }

        public async Task<IActionResult> Download(Guid id)
        {
            var user = this.HttpContext.Items["User"] as User;
            var fileTask = _fileService.GetFile(id);
            var file = (await fileTask);
            if (file.UserId == user.Id) {
                var content = _fileService.GetFileContent(id);

                return File(await content, "text/csv", file.GivenFilename);
            }
            return NotFound();
        }

        public async Task<IActionResult> UploadFile(FileUploadViewModel upload)
        {
            BusinessLogic.Contracts.Models.File newFile = null;
            try
            {
                var user = this.HttpContext.Items["User"] as User;
                byte[] content = null;
                using (var memStream = new MemoryStream())
                {
                    await upload.UploadedFile.CopyToAsync(memStream);
                    content = memStream.ToArray();
                }
                var filenameParts = upload.UploadedFile.FileName.Split('.');
                newFile = await _fileService.SaveFile(new BusinessLogic.Contracts.Models.File
                {
                    Id = upload.Id,
                    Extension = filenameParts[filenameParts.Length-1],
                    GivenFilename = upload.UploadedFile.FileName,
                    Content = content,
                    UserId = user.Id
                });
            }
            catch (ArgumentException)
            {
                return View("FileStatus",new FileStatusViewModel { Success = false, Message = "The file is not a csv" });
            }
            catch (IOException)
            {
                return View("FileStatus", new FileStatusViewModel { Success = false, Message = "File failed to save!" });
            }
            return View("FileStatus", new FileStatusViewModel { Success = true, Message = "File succesfully saved.", FileId =newFile.Id });
        }
    }
}