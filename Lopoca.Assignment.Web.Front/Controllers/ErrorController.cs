﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Lopoca.Assignment.Web.Front.Controllers
{
    public class ErrorController : Controller
    {
        [Route("[controller]/[action]")]
        public IActionResult Forbidden()
        {
            Response.StatusCode = 403;
            return View("StatusCode",403);
        }
        [Route("[controller]/[action]/{code}")]
        public IActionResult Status(int code)
        {
            Response.StatusCode = code;
            return View("StatusCode", code);
        }
    }
}