﻿using Lopoca.Assignment.BusinessLogic.Contracts;
using Lopoca.Assignment.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lopoca.Assignment.Web.Front.AppData
{
    public class DataInitialiser
    {
        private readonly IUserRepository _userRepository;
        private readonly IEncryptionProvider _encryptionProvider;

        public DataInitialiser(IUserRepository userRepository, IEncryptionProvider encryptionProvider)
        {
            _userRepository = userRepository;
            _encryptionProvider = encryptionProvider;
        }

        public async void Init()
        {
            if ((await _userRepository.Get("a7b94636-28c8-4f19-b7cd-f0732e1a483c")) == null)
            {
                var rnd = new Random();
                var salt = _encryptionProvider.Encrypt(rnd.Next(100000).ToString());

                var usrReq = _userRepository.Insert(new Assignment.Data.Contracts.Models.User
                {
                    Id = Guid.Parse("a7b94636-28c8-4f19-b7cd-f0732e1a483c"),
                    Login = "Test1",
                    Password = _encryptionProvider.Encrypt("Test" + salt),
                    PasswordSalt = salt,
                });

                salt =  _encryptionProvider.Encrypt(rnd.Next(100000).ToString());

                usrReq = _userRepository.Insert(new Assignment.Data.Contracts.Models.User
                {
                    Id = Guid.NewGuid(),
                    Login = "Test2",
                    Password = _encryptionProvider.Encrypt("Test" + salt),
                    PasswordSalt = salt,
                });
                salt = _encryptionProvider.Encrypt(rnd.Next(100000).ToString());

                usrReq = _userRepository.Insert(new Assignment.Data.Contracts.Models.User
                {
                    Id = Guid.NewGuid(),
                    Login = "Test3",
                    Password = _encryptionProvider.Encrypt("Test" + salt),
                    PasswordSalt = salt,
                }); salt = _encryptionProvider.Encrypt(rnd.Next(100000).ToString());

                usrReq = _userRepository.Insert(new Assignment.Data.Contracts.Models.User
                {
                    Id = Guid.NewGuid(),
                    Login = "Test4",
                    Password = _encryptionProvider.Encrypt("Test" + salt),
                    PasswordSalt = salt,
                }); salt = _encryptionProvider.Encrypt(rnd.Next(100000).ToString());

                usrReq = _userRepository.Insert(new Assignment.Data.Contracts.Models.User
                {
                    Id = Guid.NewGuid(),
                    Login = "Test5",
                    Password = _encryptionProvider.Encrypt("Test" + salt),
                    PasswordSalt = salt,
                });
            }
        }
    }
}
