﻿using Lopoca.Assignment.Web.Middleware;
using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lopoca.Assignment.Web.Piplines
{
    public class AuthenticatedPipline
    {
        public void Configure(IApplicationBuilder applicationBuilder)
        {
            //applicationBuilder.
            applicationBuilder.UseMiddleware<CookieAuthMiddleware>();
        }
    }
}
