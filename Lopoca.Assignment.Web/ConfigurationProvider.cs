﻿using Lopoca.Assignment.Common;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lopoca.Assignment.Web
{
    public class ConfigurationProvider : Common.IConfigurationProvider
    {
        private readonly IConfiguration _config;

        public ConfigurationProvider(IConfiguration config)
        {
            _config = config;
        }
        public string GetSetting(string key)
        {
            return _config["Settings:"+key];
        }
        public string GetConnectionString(string key)
        {
            return _config.GetConnectionString(key);
        }
    }
}
