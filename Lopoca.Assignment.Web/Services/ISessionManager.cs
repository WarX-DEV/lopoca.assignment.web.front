﻿using Lopoca.Assignment.BusinessLogic.Contracts.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lopoca.Assignment.Web.Services
{
    public interface ISessionManager
    {
        void RemoveSession();
        Task<Session> GetSession();
        Task CreateSession(Session userId);
        Task<bool> IsValidSession();
        Task ExtendSession(int minutes);
    }
}
