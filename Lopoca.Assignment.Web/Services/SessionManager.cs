﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;
using Lopoca.Assignment.BusinessLogic.Contracts;
using System.Threading.Tasks;
using Lopoca.Assignment.BusinessLogic.Contracts.Models;

namespace Lopoca.Assignment.Web.Services
{
    public class SessionManager : ISessionManager
    {
        private const string CookieKey = "X-Session-Key";
        private readonly IAuthService _authService;
        private readonly IHttpContextAccessor _httpContext;

        public SessionManager(IAuthService authService, IHttpContextAccessor context)
        {
            _authService = authService;
            _httpContext = context;
        }

        private string GetCookieValue()
        {
            _httpContext.HttpContext.Request.Cookies.TryGetValue(CookieKey, out string key);
            return key;
        }

        public async Task<bool> IsValidSession()
        {
            var key = GetCookieValue();
            if (!string.IsNullOrEmpty(key))
            {
                var session = await _authService.CheckToken(Guid.Parse(key));
                if (session != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public async void RemoveSession()
        {
            _httpContext.HttpContext.Response.Cookies.Delete(CookieKey);
        }

        public async Task ExtendSession(int minutes)
        {
            var cookieValue = GetCookieValue();
            var session = Guid.Parse(cookieValue);
            await _authService.ExtendSession(session, minutes);
        }

        public async Task CreateSession(Session session)
        {
            _httpContext.HttpContext.Response.Cookies.Append(CookieKey, session.Id.ToString());
        }

        public async Task<Session> GetSession()
        {
            var key = GetCookieValue();
            if (!string.IsNullOrEmpty(key))
            {
                return await _authService.CheckToken(Guid.Parse(key));
            }
            return null;
        }
    }
}
