﻿using Lopoca.Assignment.BusinessLogic.Contracts;
using Lopoca.Assignment.Web.Services;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lopoca.Assignment.Web.Middleware
{
    public class CookieAuthMiddleware
    {
        private readonly ISessionManager _sessionManager;
        private readonly RequestDelegate _next;

        public CookieAuthMiddleware(RequestDelegate next, ISessionManager sessionManager)
        {
            _sessionManager = sessionManager;
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (await _sessionManager.IsValidSession())
            {
                await _sessionManager.ExtendSession(15);
                var session = await _sessionManager.GetSession();
                if (!httpContext.Items.ContainsKey("User"))
                {
                    httpContext.Items.Add("Session", session);
                    httpContext.Items.Add("User", session.User);
                }
                await _next(httpContext);
            }
            else
            {
                httpContext.Response.Redirect("/Error/Forbidden");
            }
        }
    }
}
