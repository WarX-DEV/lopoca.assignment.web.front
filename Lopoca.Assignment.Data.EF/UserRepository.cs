﻿using Lopoca.Assignment.Data.Contracts;
using System;
using Lopoca.Assignment.Data.Contracts.Models;
using System.Threading.Tasks;
using System.Linq;
using AutoMapper;
using Lopoca.Assignment.Data.EF.Models;

namespace Lopoca.Assignment.Data.EF
{
    public class UserRepository : BaseRepository,IUserRepository
    {
        public UserRepository(AssignmentContext context, IMapper mapper) : base(context, mapper) { }

        public async Task Delete(Guid id)
        {
            var user = Context.Users.Where(x => x.Id.Equals(id)).FirstOrDefault();
            if (user != null)
            {
                user.IsDeleted = true;
                Context.SaveChanges();
            }
        }

        public async Task<User> Get(Guid id)
        {
            var user = Context.Users.Where(x => x.Id.Equals(id)).FirstOrDefault();
            return Mapper.Map<User>(user);
        }

        public async Task<User> Get(string login)
        {
            var user = Context.Users.Where(x => x.Login.Equals(login)).FirstOrDefault();
            return Mapper.Map<User>(user);
        }

        public async Task Insert(User user)
        {
            var newUser = Mapper.Map<UserDTO>(user);
            var existingUser = Context.Users.FirstOrDefault(x => x.Id.Equals(newUser.Id));
            if(existingUser == null)
            {
                await Context.Users.AddAsync(newUser);
                Context.SaveChanges();
            }
        }

        public async Task Update(User user)
        {
            var updatedUser = Mapper.Map<UserDTO>(user);
            var existingUser = Context.Users.FirstOrDefault(x => x.Id.Equals(updatedUser.Id));
            existingUser.Name = user.Name;
            existingUser.Surname = user.Surname;
            existingUser.Password = user.Password;
            existingUser.PasswordSalt = user.PasswordSalt;

            if (existingUser != null)
            {
                Context.SaveChanges();
            }
        }
    }
}
