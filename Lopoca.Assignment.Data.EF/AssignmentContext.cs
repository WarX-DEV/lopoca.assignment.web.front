﻿using Lopoca.Assignment.Common;
using Lopoca.Assignment.Data.EF.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lopoca.Assignment.Data.EF
{
    public class AssignmentContext : DbContext
    {
        private readonly IConfigurationProvider _configurationProvider;

        public AssignmentContext(DbContextOptions<AssignmentContext> options,IConfigurationProvider configurationProvider) : base(options)
        {
            _configurationProvider = configurationProvider;
        }

        public DbSet<FileDTO> Files { get; set; }
        public DbSet<SessionDTO> Sessions { get; set; }
        public DbSet<UserDTO> Users { get; set; }
        public DbSet<AuditDTO> Audit { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configurationProvider.GetConnectionString("DefaultConnection"));
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserDTO>().HasKey(e => e.Id);
            modelBuilder.Entity<UserDTO>().HasQueryFilter(e => !e.IsDeleted);

            base.OnModelCreating(modelBuilder);
        }
    }
}
