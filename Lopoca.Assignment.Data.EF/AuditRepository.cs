﻿using Lopoca.Assignment.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using Lopoca.Assignment.Data.Contracts.Models;
using System.Threading.Tasks;
using AutoMapper;
using Lopoca.Assignment.Data.EF.Models;

namespace Lopoca.Assignment.Data.EF
{
    public class AuditRepository : BaseRepository,IAuditRepository
    {
        public AuditRepository(AssignmentContext assignmentContext,IMapper mapper):base(assignmentContext, mapper)
        {

        }
        public async Task Log(AuditEntry auditEntry)
        {
            var dbEntry = Mapper.Map<AuditDTO>(auditEntry);
            Context.Audit.Add(dbEntry);
            Context.SaveChanges();
        }
    }
}
