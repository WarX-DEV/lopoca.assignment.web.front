﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lopoca.Assignment.Data.EF
{
    public abstract class BaseRepository
    {
        private readonly AssignmentContext _context;
        private readonly IMapper _mapper;

        protected AssignmentContext Context { get { return _context; } }
        protected IMapper Mapper { get { return _mapper; } }


        public BaseRepository(AssignmentContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    }
}
