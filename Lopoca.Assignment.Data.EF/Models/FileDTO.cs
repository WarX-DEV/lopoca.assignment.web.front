﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Lopoca.Assignment.Data.EF.Models
{
    public class FileDTO
    {
        [Key]
        public Guid Id { get; set; } = new Guid();
        public string GivenFilename { get; set; }
        public string Extension { get; set; }
        public Guid UserId { get; set; }
    }
}