﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lopoca.Assignment.Data.EF.Models
{
    public class AuditDTO
    {
        public Guid Id { get; set; }
        public DateTime Timestamp { get; set; }
        public Guid AffectedObject { get; set; }
        public string Message { get; set; }
    }
}
