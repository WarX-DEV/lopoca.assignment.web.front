﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Lopoca.Assignment.Data.EF.Models
{
    public class SessionDTO
    {
        [Key]
        public Guid Id { get; set; } = new Guid();
        public DateTime Expiry { get; set; }
        public Guid UserId { get; set; }
    }
}
