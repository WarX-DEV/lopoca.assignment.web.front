﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Lopoca.Assignment.Data.EF.Models
{
    public class UserDTO
    {
        [Key]
        public Guid Id { get; set; } = new Guid();
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public bool IsDeleted { get; internal set; }
    }
}