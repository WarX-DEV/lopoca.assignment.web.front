﻿using Lopoca.Assignment.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using Lopoca.Assignment.Data.Contracts.Models;
using System.Threading.Tasks;
using AutoMapper;
using Lopoca.Assignment.Common;
using System.IO;
using System.Linq;
using Lopoca.Assignment.Data.EF.Models;

namespace Lopoca.Assignment.Data.EF
{
    public class FileRepository : BaseRepository, IFileRepository
    {
        private readonly Common.IConfigurationProvider _configProvider;
        private readonly string rootPath;
        private readonly IAuditRepository _audit;

        public FileRepository(AssignmentContext context, Lopoca.Assignment.Common.IConfigurationProvider configurationProvider, IMapper mapper, IAuditRepository audit) : base(context, mapper)
        {
            _configProvider = configurationProvider;
            rootPath = _configProvider.GetSetting("rootPath");
            _audit = audit;
        }

        public async Task Delete(Guid id)
        {
            var entry = Context.Files.First(x => x.Id.Equals(id));

            //enable the following if you need to delete files on deleteion of file entry
            //currently not deleting the files not to lose anything
            //var rootPath = _configProvider.GetSetting("rootPath");
            //var file = $@"{rootPath}\{entry.UserId}\{entry.StoredName}";
            //File.Delete(file);

            Context.Files.Remove(entry);

        }

        public async Task<FileData> Get(Guid id)
        {
            var entry = internalGet(id);

            var fileData = Mapper.Map<FileData>(entry);

            return fileData;
        }

        private FileDTO internalGet(Guid id)
        {
            return Context.Files.First(x => x.Id.Equals(id));
        }

        public async Task<IEnumerable<FileData>> GetByUser(Guid userId)
        {
            var entries = Context.Files.Where(x => x.UserId.Equals(userId)).ToList();

            var files = Mapper.Map<IEnumerable<FileData>>(entries);

            await _audit.Log(new AuditEntry
            {
                AffectedObject = userId,
                Message = "Access file list"
            });

            return files;
        }

        public async Task<byte[]> GetContent(Guid id)
        {
            var entry = Context.Files.First(x => x.Id.Equals(id));
            var file = $@"{rootPath}\{entry.UserId}\{entry.Id}.{entry.Extension}";
            await _audit.Log(new AuditEntry
            {
                AffectedObject = id,
                Message = "File read"
            });
            return File.ReadAllBytes(file);
        }

        public async Task<FileData> Insert(FileData file)
        {
            var dbFile = Mapper.Map<FileDTO>(file);
            dbFile.Id = Guid.NewGuid();
            SafeCreateUserFolder($@"{rootPath}\{file.UserId}\");
            File.WriteAllBytes($@"{rootPath}\{file.UserId}\{dbFile.Id}.{file.Extension}", file.Content);
            await Context.Files.AddAsync(dbFile);
            Context.SaveChanges();
            await _audit.Log(new AuditEntry
            {
                AffectedObject = dbFile.Id,
                Message = "File inserted"
            });
            return Mapper.Map<FileData>(dbFile);
        }

        public async Task<FileData> Update(FileData file)
        {
            File.WriteAllBytes($@"{rootPath}\{file.UserId}\{file.Id}.{file.Extension}", file.Content);
            var dbFile = internalGet(file.Id);
            dbFile.GivenFilename = file.GivenFilename;
            dbFile.Extension = file.Extension;
            Context.SaveChanges();
            return Mapper.Map<FileData>(dbFile);
        }

        private void SafeCreateUserFolder(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }
    }
}
