﻿using Lopoca.Assignment.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using Lopoca.Assignment.Data.Contracts.Models;
using System.Threading.Tasks;
using AutoMapper;
using System.Linq;
using Lopoca.Assignment.Data.EF.Models;

namespace Lopoca.Assignment.Data.EF
{
    public class SessionRepository : BaseRepository, ISessionRepository
    {
        public SessionRepository(AssignmentContext context, IMapper mapper) : base(context, mapper) { }

        public async Task Delete(Guid id)
        {
        }

        public async Task<Session> Get(Guid id)
        {
            return Mapper.Map<Session>(internalGet(id));
        }

        private SessionDTO internalGet(Guid id)
        {
            return Context.Sessions.FirstOrDefault(x => x.Id.Equals(id));
        }

        public async Task Insert(Session session)
        {
            var dbSession = Mapper.Map<SessionDTO>(session);
            await Context.Sessions.AddAsync(dbSession);
            Context.SaveChanges();
        }

        public async Task Update(Session session)
        {
            var dbSession = internalGet(session.Id);
            dbSession.Expiry = session.Expiry;
            Context.SaveChanges();
        }
    }
}
